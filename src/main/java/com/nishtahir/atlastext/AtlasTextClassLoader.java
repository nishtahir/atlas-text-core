package com.nishtahir.atlastext;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Class loader to load language tools dynamically
 * at runtime, this helps decouple to the project into
 * more manageable components.
 */
public class AtlasTextClassLoader {

    /**
     *
     */
    private static AtlasTextClassLoader instance;

    /**
     *
     */
    private List<IParsable> parsables;

    /**
     *
     */
    private AtlasTextClassLoader() {
        parsables = new ArrayList<>();
    }

    /**
     * @return
     */
    public static AtlasTextClassLoader getInstance() {
        if (instance == null) {
            instance = new AtlasTextClassLoader();
        }
        return instance;
    }

    /**
     * @param path
     * @throws FileNotFoundException
     */
    public void load(String path) throws FileNotFoundException {
        File file = new File(path);
        if (!file.exists()) {
            throw new FileNotFoundException("Path not found");
        }
        try {
            load(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param files
     * @throws Exception
     */
    public void load(URL[] files) throws Exception {
        URLClassLoader child = new URLClassLoader(files, this.getClass().getClassLoader());
        InputStream stream = child.getResourceAsStream("config.properties");
        Properties properties = new Properties();
        properties.load(stream);
        String config = properties.getProperty("parsable");
        Class<IParsable> clazz = (Class<IParsable>) Class.forName(config, true, child);
        IParsable instance = clazz.newInstance();
        parsables.add(instance);
    }

    /**
     *
     * @param file
     * @throws Exception
     */
    public void load(File file) throws Exception {
        if (file.isDirectory()) {
            for (File jar : FileUtils.listFiles(file, getExtensions(), false)) {
                load(jar);
            }
        } else {
            load(new URL[]{file.toURI().toURL()});
        }
    }

    /**
     * List of {@link IParsable} that have been loaded
     *
     * @return
     */
    public List<IParsable> getParsables() {
        return parsables;
    }

    public static String[] getExtensions() {
        return new String[]{"jar"};
    }
}
