package com.nishtahir.atlastext;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;

/**
 *
 */
public interface IParsable {

    Parser getParser();

    Lexer getLexer(CharStream input);
    
}
