package com.nishtahir.atlastext;

import org.junit.Test;

import java.io.File;

/**
 * Created by nish on 3/21/16.
 */
public class AtlasTextClassLoaderTest {

    @Test
    public void testLoad() throws Exception {
        AtlasTextClassLoader loader = AtlasTextClassLoader.getInstance();
        loader.load(new File("atlas-text-java-1.0-SNAPSHOT.jar"));
    }
}